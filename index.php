<?php
require_once 'products.php';

use \cart\Cart;
use \cart\exchange\Exchange;


function __autoload($class){
    $path = str_replace('\\','/', $class).'.php';
    if(file_exists($path)){
        require $path;
    }
}

$product = new Cart();
$change = new Exchange();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div>
    <div class="container">Корзина:<br>
    <?php if(!empty($product->getProducts())):?>
        <?php foreach($product->getProducts() as $cartItem):?>
            <?=$cartItem->name ?> - <?=$change->convert($cartItem->price, 'usd') ?><br>
           <!-- <p>Количество</p>-->
        <?php endforeach;?>
    <?php else:?>
        <h4>Корзина пуста</h4>
    <?php endif; ?>
    </div>
</div>

<div class="container">
    <div class="card-deck mb-3 text-center">
        <?php foreach($products as $id => $product):?>
            <div class="col-xs-12 col-md-4 col-lg-5">
                <div class="card-header">
                    <h4 class="my-0 font-weight-normal"><?=$product['name']?></h4>
                </div>
                <div class="card-body">
                    <h1 class="card-title pricing-card-title"><?=$product['price']?> <small class="text-muted">грн</small></h1>
                    <p><?=$product['description']?></p>
                    Цена: <?=$change->convert($product['price'], 'uah')?><br>
                    <a href="add2cart.php?id=<?=$id ?>" type="button">Купить </a>

                </div>
            </div>
        <?endforeach;?>
    </div>
</div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


</body>
</html>
