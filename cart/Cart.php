<?php
namespace cart;
/*header('Content-Type: text/html; charset=utf-8');*/
class Cart
{
    protected $products;

    public function __construct()
    {
        if (isset($_COOKIE['cart'])) {
            $this->products = json_decode($_COOKIE['cart']);

        } else {
            $this->products = array();
        }

    }

    public function getProducts()
    {
        return $this->products;
    }

    public function saveCart()
    {
        $value = json_encode($this->products);
        setcookie('cart', $value, time() + 300);
    }

    public function addProduct($prod)
    {
        if (!in_array($prod, $this->products, true) ) {
            array_push($this->products, $prod);
        }

    }
}


