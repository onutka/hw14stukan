<?php

namespace cart\exchange;

class Exchange
{
    protected $price;
    protected $curr;

 public  function convert ($price, $yourCurr)
 {
     if($yourCurr == 'usd'){
         $final = round($price / 29,1);
         return $final.' USD';
     }elseif ($yourCurr == 'eur'){
         $final = round($price / 35,1);
         return $final.' EUR';
    }else{
         return $price.' UAH';
     }
 }
}